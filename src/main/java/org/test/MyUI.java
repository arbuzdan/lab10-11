package org.test;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.event.MouseEvents;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.*;
import org.w3c.dom.Text;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of an HTML page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        final VerticalLayout layout = new VerticalLayout();
        final HorizontalLayout horizontalLayout = new HorizontalLayout();
        final HorizontalLayout horizontalLayout1 = new HorizontalLayout();
        final HorizontalLayout horizontalLayout2 = new HorizontalLayout();
        final HorizontalLayout horizontalLayout3 = new HorizontalLayout();
        final HorizontalLayout horizontalLayout4 = new HorizontalLayout();

        final TextField surname = new TextField();
        surname.setCaption("Type your surname here:");
        final TextField name = new TextField();
        name.setCaption("Type your name here:");
        final TextField tf = new TextField("A Field");
        tf.setValue("Stuff in the field");
        final TextField tf1 = new TextField("My Eventful Field");
        tf1.setValue("Initial content");
        tf1.setMaxLength(20);
        final TextField nS = new TextField();
        nS.setCaption("Type your name: ");
        final TextField surN = new TextField();
        surN.setCaption("Type your surname: ");
        final TextField date = new TextField();
        date.setCaption("Type your date of birth: ");

        Button button = new Button("Click Me");
        Button lol = new Button("Dont Click Me");

        List<Person> list = new ArrayList<>();
        list.add(new Person("Danila", "Arbuzov", "2308"));
        list.add(new Person("Krzysztof",  "Majda", "4387"));
        list.add(new Person("Andrey", "Arbuzov", "8234"));

        Grid<Person> grid = new Grid<>();
        grid.setItems(list);
        grid.addColumn(Person::getName).setCaption("Name");
        grid.addColumn(Person::getSurname).setCaption("Surname");
        grid.addColumn(Person::getBd).setCaption("Year of birth");



        Label counter = new Label();
        counter.setValue(tf1.getValue().length() +
                " of " + tf1.getMaxLength());

        CheckBox checkbox1 = new CheckBox("Box with no Check");
        CheckBox checkbox2 = new CheckBox("Box with a Check");
        CheckBox checkbox3 = new CheckBox("Do you want to add surname?");
        checkbox3.setValue(true);
        checkbox2.setValue(true);
        checkbox1.addValueChangeListener(event ->
                checkbox2.setValue(! checkbox1.getValue()));

        final Button upload = new Button("Upload Data",
                new Button.ClickListener() {
                    @Override
                    public void buttonClick(Button.ClickEvent event) {
                        if(checkbox3.getValue() == true) {
                            Person person = new Person(nS.getValue(), surN.getValue(), date.getValue());
                            list.add(person);
                            grid.setItems(list);
                        }
                        else{
                            Person person = new Person(nS.getValue(), date.getValue());
                            list.add(person);
                            grid.setItems(list);
                        }

                        // Person person = new Person();
                    }
                });


        tf.setValueChangeMode(ValueChangeMode.EAGER);
        tf.addValueChangeListener(event ->
                Notification.show("Value is: " + event.getValue()));

        tf1.addValueChangeListener(event -> {
            int len = event.getValue().length();
            counter.setValue(len + " of " + tf1.getMaxLength());
        });

        lol.addClickListener(e -> {
            horizontalLayout2.addComponent(new Label("I said dont click me tho!"));
        });

        button.addClickListener(e -> {
            horizontalLayout2.addComponent(new Label("Thanks " + name.getValue() + " "
                    + surname.getValue() + ", it works!"));
        });

        horizontalLayout.addComponents(nS, surN, date);
        horizontalLayout1.addComponents(name, surname);
        horizontalLayout2.addComponents(button, lol);
        horizontalLayout3.addComponents(checkbox1, checkbox2);
        horizontalLayout4.addComponents(tf, tf1);
        layout.addComponents(horizontalLayout1, horizontalLayout2, horizontalLayout3, horizontalLayout4, counter, grid, horizontalLayout, checkbox3, upload);
        
        setContent(layout);
    }

    @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}
