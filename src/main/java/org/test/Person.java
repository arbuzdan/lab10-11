package org.test;

public class Person {

    private int id;
    private String name;
    private String bd;
    private String surname;

    public Person() {

    }

    public Person(String name, String surname, String bd) {
        this.name = name;
        this.surname = surname;
        this.bd = bd;
    }

    public Person(String name, String bd){
        this.name = name;
        this.bd = bd;
    }

    public Person(int id, String name, String bd) {
        this.id = id;
        this.name = name;
        this.bd = bd;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSurname(String surname){
        this.surname = surname;
    }

    public void setBd(String bd) {
        this.bd = bd;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() { return surname; }

    public String getBd() {
        return bd;
    }

    @Override
    public String toString() {
        return "[id: " + id + ", name: " + name + ", surname:" + surname + ", bd: " + bd + "]";
    }
}
