//package org.test;
//
//import java.sql.Connection;
//import java.sql.Driver;
//import java.sql.DriverManager;
//import java.sql.SQLException;
//
//public class DBPerson {
//
//    private static final String URL = "jdbc:mysql://localhost:3306/mydbtest";
//    private static final String USERNAME = "root";
//    private static final String PASSWORD = "root";
//
//    Connection connection;
//
//    public DBPerson() {
//
//        try {
//            Driver driver = new com.mysql.cj.jdbc.Driver();
//            DriverManager.registerDriver(driver);
//        } catch (SQLException e) {
//            System.out.println("Подключиться к базе данных не удалось");
//        }
//
//        try {
//            connection = DriverManager.getConnection(URL, USERNAME, PASSWORD);
//        } catch (SQLException e) {
//            System.out.println("Установить соединение не удалось");
//        }
//    }
//
//    public Connection getConnection(){
//        return connection;
//    }
//}
